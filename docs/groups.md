# Groups
`setux.managers.common.groups`

[Setux] User Groups manangement

[PyPI] - [Repo] - [Doc]


ArgsChecker [Manager] implementation

`setux.core.manage.ArgsChecker`



[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest/groups
[Setux]: https://setux.readthedocs.io/en/latest

[Manager]: https://setux-core.readthedocs.io/en/latest/manage
