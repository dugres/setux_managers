# Group
`setux.managers.common.group`

[Setux] Group manangement

[PyPI] - [Repo] - [Doc]


SpecChecker [Manager] implementation

`setux.core.manage.SpecChecker`



[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest/group
[Setux]: https://setux.readthedocs.io/en/latest

[Manager]: https://setux-core.readthedocs.io/en/latest/manage
