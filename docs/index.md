# Setup Managers

[Setux] Managers

[PyPI] - [Repo] - [Doc]

## Installation

    $ pip install setux_managers


[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest
[Setux]: https://setux.readthedocs.io/en/latest
