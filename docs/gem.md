# Gem
`setux.managers.common.gem`

[Setux] Ruby Packages Manager

[PyPI] - [Repo] - [Doc]


[CommonPackager] implementation

`setux.core.package.CommonPackager`



[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest/gem
[Setux]: https://setux.readthedocs.io/en/latest

[Manager]: https://setux-core.readthedocs.io/en/latest/manage
