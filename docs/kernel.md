# Kernel
`setux.managers.common.kernel`

[Setux] Kernel infos.

[PyPI] - [Repo] - [Doc]


[Manager] implementation

`setux.core.manage.Manager`



[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest/kernel
[Setux]: https://setux.readthedocs.io/en/latest

[Manager]: https://setux-core.readthedocs.io/en/latest/manage
