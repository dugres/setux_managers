# Service
`setux.managers.system.package`

[Setux] System Services management

[PyPI] - [Repo] - [Doc]


[Service] implementation

`setux.core.service.Service`



[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest/service
[Setux]: https://setux.readthedocs.io/en/latest

[Service]: https://setux-core.readthedocs.io/en/latest/service
