# Pip
`setux.managers.common.pip`

[Setux] Python Packages Manager

[PyPI] - [Repo] - [Doc]


[CommonPackager] implementation

`setux.core.package.CommonPackager`



[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest/pip
[Setux]: https://setux.readthedocs.io/en/latest

[Manager]: https://setux-core.readthedocs.io/en/latest/manage
