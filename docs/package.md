# Package
`setux.managers.system.package`

[Setux] System Packages management

[PyPI] - [Repo] - [Doc]


[SystemPackager] implementation

`setux.core.package.SystemPackager`



[PyPI]: https://pypi.org/project/setux_managers
[Repo]: https://bitbucket.org/dugres/setux_managers
[Doc]: https://setux-managers.readthedocs.io/en/latest/package
[Setux]: https://setux.readthedocs.io/en/latest

[SystemPackager]: https://setux-core.readthedocs.io/en/latest/package
